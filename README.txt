ADDING HOTDOC BLOCKS
------------------

To add new menu blocks, use the "Add HotDoc menu block" link on the administer blocks
page, admin/structure/block. You will then be able to configure your menu block
before adding it.

CONFIGURING HOTDOC BLOCKS
-----------------------

Configuration for HotDoc widgets are based upon the Clinic and Suburb settings with your
HotDoc implementation.  These settings can be gathered via your HotDoc account or gleamed
via the URL patterns from your online booking page at HotDoc via the following convention - 

https://www.hotdoc.com.au/medical-centres/%%Clinic-Suburb%%/%%Clinic-Name%%/doctors

The following configurations options are set via the adding or editing the blocks - 

Clinic Name (required)
The name of the Clinic as it appears in HotDoc.

Clinic Suburb (required)
The Suburb in which the Clinic is listed in at HotDoc.

HotDoc Iframe Height (required)
The px or % height of the rendered iframe.  Must include 'px' or '%' in value.
