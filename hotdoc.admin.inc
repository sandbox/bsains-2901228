<?php
/**
 * @file
 * Provides infrequently used functions and hooks for menu_block.
 */

/**
 * Menu callback: display the hotdoc block addition form.
 *
 * @see hotdoc_add_block_form_submit()
 */
function hotdoc_add_block_form($form, &$form_state) {
    module_load_include('inc', 'block', 'block.admin');
    $form = block_admin_configure($form, $form_state, 'hotdoc', NULL);

    // Other modules should be able to use hook_form_block_add_block_form_alter()
    // to modify this form, so add a base form ID.
    $form_state['build_info']['base_form_id'] = 'block_add_block_form';

    // Prevent block_add_block_form_validate/submit() from being automatically
    // added because of the base form ID by providing these handlers manually.
    $form['#validate'] = array();
    $form['#submit'] = array('hotdoc_add_block_form_submit');

    return $form;
}

/**
 * Save the new hotdoc block.
 */
function hotdoc_add_block_form_submit($form, &$form_state) {
    // Determine the delta of the new block.
    $block_ids = variable_get('hotdoc_ids', array());
    $delta = empty($block_ids) ? 1 : max($block_ids) + 1;
    $form_state['values']['delta'] = $delta;

    // Save the new array of blocks IDs.
    $block_ids[] = $delta;
    variable_set('hotdoc_ids', $block_ids);

    // Save the block configuration.
    hotdoc_block_save($delta, $form_state['values']);

    // Run the normal new block submission (borrowed from block_add_block_form_submit).
    $query = db_insert('block')->fields(array('visibility', 'pages', 'custom', 'title', 'module', 'theme', 'region', 'status', 'weight', 'delta', 'cache'));
    foreach (list_themes() as $key => $theme) {
        if ($theme->status) {
            $region = !empty($form_state['values']['regions'][$theme->name]) ? $form_state['values']['regions'][$theme->name] : BLOCK_REGION_NONE;
            $query->values(array(
                    'visibility' => (int) $form_state['values']['visibility'],
                    'pages' => trim($form_state['values']['pages']),
                    'custom' => (int) $form_state['values']['custom'],
                    'title' => $form_state['values']['title'],
                    'module' => $form_state['values']['module'],
                    'theme' => $theme->name,
                    'region' => ($region == BLOCK_REGION_NONE ? '' : $region),
                    'status' => 0,
                    'status' => (int) ($region != BLOCK_REGION_NONE),
                    'weight' => 0,
                    'delta' => $delta,
                    'cache' => DRUPAL_CACHE_PER_ROLE,
            ));
        }
    }
    $query->execute();

    $query = db_insert('block_role')->fields(array('rid', 'module', 'delta'));
    foreach (array_filter($form_state['values']['roles']) as $rid) {
        $query->values(array(
                'rid' => $rid,
                'module' => $form_state['values']['module'],
                'delta' => $delta,
        ));
    }
    $query->execute();

    drupal_set_message(t('The block has been created.'));
    cache_clear_all();
    $form_state['redirect'] = 'admin/structure/block';
}

/**
 * Alters the block admin form to add delete links next to menu blocks.
 */
function _hotdoc_form_block_admin_display_form_alter(&$form, $form_state) {
   
    foreach (variable_get('hotdoc_ids', array()) as $delta) {     
            $form['blocks']['hotdoc_' . $delta]['delete'] = array('#type' => 'link', '#title' => t('delete'), '#href' => 'admin/structure/block/delete-hotdoc/' . $delta);
    }
}

/**
 * Menu callback: confirm deletion of menu blocks.
 */
function hotdoc_delete_form($form, &$form_state, $delta = 0) {
    $title = _hotdoc_format_title(hotdoc_get_config($delta));
    $form['block_title'] = array('#type' => 'hidden', '#value' => $title);
    $form['delta'] = array('#type' => 'hidden', '#value' => $delta);

    return confirm_form($form, t('Are you sure you want to delete the "%name" block?', array('%name' => $title)), 'admin/structure/block', NULL, t('Delete'), t('Cancel'));
}

/**
 * Deletion of menu blocks.
 */
function hotdoc_delete_form_submit($form, &$form_state) {
    // Remove the menu block configuration variables.
    $delta = $form_state['values']['delta'];
    hotdoc_delete($delta);
    drupal_set_message(t('The block "%name" has been removed.', array('%name' => $form_state['values']['block_title'])));
    cache_clear_all();
    $form_state['redirect'] = 'admin/structure/block';
    return;
}

/**
 * Implements hook_block_info().
 */
function _hotdoc_block_info() {
    $blocks = array();
    $deltas = variable_get('hotdoc_ids', array());
    foreach ($deltas as $delta) {
        $blocks[$delta]['info'] = _hotdoc_format_title(hotdoc_get_config($delta));
        $blocks[$delta]['cache'] = DRUPAL_CACHE_PER_ROLE;
    }
    return $blocks;
}

/**
 * Return the title of the block.
 *
 * @param $config
 *   array The configuration of the menu block.
 * @return
 *   string The title of the block.
 */
function _hotdoc_format_title($config) {
    // If an administrative title is specified, use it.
    if (!empty($config['admin_title'])) {
        return check_plain($config['admin_title']);
    }
    $title = 'HotDoc Widget - ' . $config['delta'];
    return $title;
}

/**
 * Implements hook_block_configure().
 */
function _hotdoc_block_configure($delta = '') {
    // Create a pseudo form state.
    $form_state = array('values' => hotdoc_get_config($delta));
    return hotdoc_configure_form(array(), $form_state);
}

/**
 * Returns the configuration form for a menu tree.
 *
 * @param $form_state
 *   array An associated array of configuration options should be present in the
 *   'values' key. If none are given, default configuration is assumed.
 * @return
 *   array The form in Form API format.
 */
function hotdoc_configure_form($form, &$form_state) {
    $config = array();
    // Get the config from the form state.
    if (!empty($form_state['values'])) {
        $config = $form_state['values'];
    }
    // Merge in the default configuration.
    $config += hotdoc_default_config();
    
    $form['admin_title'] = array(
            '#type' => 'textfield',
            '#default_value' => $config['admin_title'],
            '#title' => t('Administrative title'),
            '#description' => t('This title will be used administratively to identify this block. If blank, the regular title will be used.'),
    );    
    $form['hotdoc_clinic'] = array(
            '#type' => 'textfield',
            '#title' => t('Clinic Name'),
            '#size' => 255,
            '#description' => t('The name of the Clinic as it appears in HotDoc.'),
            '#default_value' => $config['hotdoc_clinic'],
            '#element_validate' => array('_hotdoc_clinic_validate'),
            '#required' => TRUE,
    );
    $form['hotdoc_suburb'] = array(
            '#type' => 'textfield',
            '#title' => t('Clinic Suburb'),
            '#size' => 255,
            '#description' => t('The Suburb in which the Clinic is listed in at HotDoc.'),
            '#default_value' => $config['hotdoc_suburb'],
            '#element_validate' => array('_hotdoc_suburb_validate'),
            '#required' => TRUE,
    );
    $form['hotdoc_iframe_height'] = array(
            '#type' => 'textfield',
            '#title' => t('HotDoc Iframe Height'),
            '#size' => 255,
            '#description' => t('The height of the iframe to be rendered.  Include \'px\' or \'%\' with value'),
            '#default_value' => $config['hotdoc_iframe_height'],
            '#element_validate' => array('_hotdoc_iframe_height_validate'),
            '#required' => TRUE,
    );

    return $form;
}
function _hotdoc_suburb_validate($element, &$form_state, $form) {
    if (empty($element['#value'])) {
        form_error($element, t('This field is required.'));
    }
}

function _hotdoc_clinic_validate($element, &$form_state, $form) {
    if (empty($element['#value'])) {
        form_error($element, t('This field is required.'));
    }
}

function _hotdoc_iframe_height_validate($element, &$form_state, $form) {
    if (empty($element['#value'])) {
        form_error($element, t('This field is required.'));
    }
    if (!preg_match('/^(\d+(%|px))$/', $element['#value'])){
        form_error($element, t('Please include the \'px\' or \'%\' with value.'));
    }
}

/**
 * Implements hook_block_save().
 */
function _hotdoc_block_save($delta = '', $edit = array()) {
    if (!empty($delta)) {
        $config = hotdoc_get_config($delta);
        variable_set("hotdoc_{$delta}_hotdoc_clinic", $edit['hotdoc_clinic']);
        variable_set("hotdoc_{$delta}_hotdoc_suburb", $edit['hotdoc_suburb']);
        variable_set("hotdoc_{$delta}_admin_title", $edit['admin_title']);
        variable_set("hotdoc_{$delta}_hotdoc_iframe_height", $edit['hotdoc_iframe_height']);
    }
}